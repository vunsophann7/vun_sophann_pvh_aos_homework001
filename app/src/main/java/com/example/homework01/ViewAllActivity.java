package com.example.homework01;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import android.content.Intent;
import android.os.Bundle;

import com.example.homework01.databinding.ActivityHomeBinding;
import com.example.homework01.databinding.ActivityViewAllBinding;

public class ViewAllActivity extends AppCompatActivity {

    ActivityViewAllBinding binding;
    User user;
    Intent intent;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_view_all);
        intent = getIntent();
        user = (User) intent.getSerializableExtra("user");
        binding.txtUsername.setText(user.getFullName());
    }
}