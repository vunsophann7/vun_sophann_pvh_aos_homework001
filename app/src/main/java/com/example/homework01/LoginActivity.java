package com.example.homework01;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.example.homework01.databinding.ActivityLoginBinding;

public class LoginActivity extends AppCompatActivity {

    ActivityLoginBinding binding;

    String userName = "";
    String memberCode = "";
    String password = "";
    String getEditPassword = "";
    User user;
    Intent intent;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        binding = DataBindingUtil.setContentView(this, R.layout.activity_login);
        intent = getIntent();
        user = (User) intent.getSerializableExtra("user");
        binding.btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getEditPassword = user.getPassword();
                if (binding.editPassword.getText().toString().equals("")) {
                    binding.editPassword.setError("Empty field");
                } else if (binding.editMemberCode.getText().toString().equals("")) {
                    binding.editMemberCode.setError("Empty field");
                } else if (binding.editUserName.getText().toString().equals("")) {
                    binding.editUserName.setError("Empty field");
                } else if (!binding.editPassword.getText().toString().equals(getEditPassword)) {
                    binding.editPassword.setError("Wrong password!!! Please try again!");
                    Toast.makeText(LoginActivity.this, ""+password+"is wrong password!!! Please try again.", Toast.LENGTH_SHORT).show();
                }else {
                    intent = getIntent();
                    user = (User) intent.getSerializableExtra("user");
                    intent = new Intent(LoginActivity.this, HomeActivity.class);
                    intent.putExtra("user", user);
                    startActivity(intent);
                }
            }
        });
    }
}