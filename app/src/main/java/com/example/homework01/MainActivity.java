package com.example.homework01;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import android.app.DatePickerDialog;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.DatePicker;
import android.widget.Toast;

import com.example.homework01.databinding.ActivityMainBinding;

import java.util.Calendar;
import java.util.Random;

public class MainActivity extends AppCompatActivity {
    ActivityMainBinding binding;
    String fullName = "";
    String password = "";
    String confirmPassword = "";
    String gender = "";
    String selectedDate;
    User user;
    Intent intent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        binding = DataBindingUtil.setContentView(this, R.layout.activity_main);
        //get data from edit text
//        user = new User();

        if (!binding.radioMale.isChecked() && !binding.radioFemale.isChecked()) {
            Toast.makeText(this, "Please select a gender", Toast.LENGTH_SHORT).show();
//            return;
        }

        binding.btnRegister.setOnClickListener(view -> {
            if (binding.txtFullName.getText().toString().isEmpty()) {
                binding.txtFullName.setError("Empty field");
            } else if (binding.txtNewPassword.getText().toString().isEmpty()) {
                binding.txtNewPassword.setError("Empty field");
            } else if (binding.txtConfirmPassword.getText().toString().isEmpty()) {
                binding.txtConfirmPassword.setError("Empty field");
            } else if (!binding.txtNewPassword.getText().toString().equals(binding.txtConfirmPassword.getText().toString())) {
                Toast.makeText(this, "Confirm password must be the same as your password", Toast.LENGTH_SHORT).show();
            } else if (!binding.radioMale.isChecked() && !binding.radioFemale.isChecked()) {
                Toast.makeText(this, "Please select a gender", Toast.LENGTH_SHORT).show();
            } else {
                fullName = binding.txtFullName.getText().toString();
                password = binding.txtNewPassword.getText().toString();
                confirmPassword = binding.txtConfirmPassword.getText().toString();

                if (binding.radioMale.isChecked()) {
                    gender = "Male";
                } else {
                    gender = "Female";
                }

                user = new User(fullName, password, gender);
                intent = new Intent(MainActivity.this, LoginActivity.class);
                intent.putExtra("user", user);
                startActivity(intent);
            }
//            call method random code in clip board
            clipBoard(4);
        });

        //get data from radio group
        binding.RadioGenderGroup.setOnCheckedChangeListener((radioGroup, checkedId) -> {
            if (checkedId == binding.radioMale.getId()) {
                gender = "Male";
            } else {
                gender = "Female";
            }
        });


        binding.btnCalandar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Calendar calendar = Calendar.getInstance();
                int Year = calendar.get(Calendar.YEAR);
                int Month = calendar.get(Calendar.MONTH);
                int Day = calendar.get(Calendar.DAY_OF_MONTH);
                DatePickerDialog datePickerDialog = new DatePickerDialog(MainActivity.this, new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker datePicker, int year, int month, int dayOfMonth) {
                        selectedDate = year + "-" + (month + 1) + "-" + dayOfMonth;
                    }
                }, Year, Month, Day);
                datePickerDialog.show();
            }
        });


    }

//    method random code in clipboard
    private String clipBoard(int length) {
        char[] chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890".toCharArray();
        StringBuilder stringBuilder = new StringBuilder();
        Random random = new Random();
        for (int i=0; i<length; i++) {
            char code = chars[random.nextInt(chars.length)];
            stringBuilder.append(code);
        }
        ClipboardManager clipboard = (ClipboardManager) getSystemService(Context.CLIPBOARD_SERVICE);
        ClipData clip = ClipData.newPlainText("label", stringBuilder.toString());
        clipboard.setPrimaryClip(clip);
        return stringBuilder.toString();
    }
}