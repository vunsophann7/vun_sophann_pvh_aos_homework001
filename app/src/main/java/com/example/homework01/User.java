package com.example.homework01;

import java.io.Serializable;

public class User implements Serializable {
    private String fullName;
    private String password;
    private String gender;

    public User(String fullName, String password, String gender) {
        this.fullName = fullName;
        this.password = password;
        this.gender = gender;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }
}
