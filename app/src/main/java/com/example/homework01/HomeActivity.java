package com.example.homework01;

//import static com.example.homework01.MainActivity.EXTRA;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.example.homework01.databinding.ActivityHomeBinding;
import com.example.homework01.databinding.ActivityMainBinding;

public class HomeActivity extends AppCompatActivity {
    ActivityHomeBinding binding;
    User user;
    Intent intent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        binding = DataBindingUtil.setContentView(this, R.layout.activity_home);
        intent = getIntent();
        user = (User) intent.getSerializableExtra("user");
        binding.txtUsername.setText(user.getFullName());
       binding.btnViewAll.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View view) {

               intent = getIntent();
               user = (User) intent.getSerializableExtra("user");
               intent = new Intent(HomeActivity.this, ViewAllActivity.class);
               intent.putExtra("user", user);
               startActivity(intent);
           }
       });

       binding.btnVisitWebsite.setOnClickListener(new View.OnClickListener() {
           Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.google.com"));
           @Override
           public void onClick(View view) {
                startActivity(intent);
           }
       });
    }
}